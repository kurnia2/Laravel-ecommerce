@extends('front-end.master')
	@section('title')
	Checkout
   @endsection
	
@section('body')
	<!--banner-->
		<div class="banner1">
			<div class="container">
				<h3><a href="{{ route('/')}}">Home</a> / <span>Sheping</span></h3>
			</div>
		</div>
	<!--banner-->

	<!--content-->
		<div class="content" style="margin-top: 30px;">
			<div class="single-w13">
				<div class="container">
					<div class="row">
						<div class="col-md-12 well">
							<h3 class="text-center text-success">Dear {{ Session::get('customerName') }}. You have to give us Product sheping info to complete your order. If your billing info and save info are same please press save sheping info button..</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-md-10 col-md-offset-1 well">
							<h3 clas="text-center text-info">Give your Sheping Info......!</h3><br/>
							<div class="form-w3agile form1">
								
								{{ Form::open(['route'=>'new-sheping', 'method'=>'post']) }}
									<div class="key">
										<i class="fa fa-user" aria-hidden="true"></i>
										<input  type="text" value="{{ $customer->first_name.' '.$customer->last_name}}" name="full_name" placeholder="Full Name" >
										<div class="clearfix"></div>
									</div>
									
									<div class="key">
										<i class="fa fa-envelope" aria-hidden="true"></i>
										<input  type="text" value="{{ $customer->email }}" name="email" placeholder="Email">
										<div class="clearfix"></div>
									</div>
									
									<div class="key">
										<i class="fa fa-phone" aria-hidden="true"></i>
										<input type="text"  value="{{ $customer->phone }}" name="phone" placeholder="Phone Number" >
										<div class="clearfix"></div>
									</div>
									<div class="form-group">
										<div class="key">
											<textarea class="form-control"  name="address" placeholder="Address" />{{ $customer->address }}</textarea>
											<div class="clearfix"></div>
										</div>
										
									</div>
									<input type="submit" name="btn" value="save sheping info">
								{{ Form::close() }}
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
@endsection