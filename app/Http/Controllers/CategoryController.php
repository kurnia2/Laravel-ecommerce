<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request; 

//For Query Builder 
//use DB;

class CategoryController extends Controller
{
    public function index(){

    	return view('admin.category.add-category');
    }
    //Insert Data 
 	
 	public function saveCategory(Request $request){

 		//Eloquent ORM method

 		$category = new Category();

 		$category->category_name         =$request->category_name;
 		$category->category_dis          =$request->category_dis;
 		$category->publication_status    =$request->publication_status;
 		$category->save();

 		/*  one comment method  */

 		// Category::create($request->all());

 		//Query Builder 
 		//DB::table('categories')->insert([

 		// 	'category_name'       => $request->category_name,
 		// 	'category_dis'        => $request->category_dis,
 		// 	'publication_status'  => $request->publication_status

 		// ]);


 		return redirect('/category/add')->with('message', 'Category info save successfully');
 	}

 	public function manageCategoryInfo(){
 		$categories = Category::all();

 		//return $categories;
 		return view('admin.category.manage-category', ['categories'=>$categories]);	
 	}
 	public function unpublishedCategoryinfo($id){

 		$category = Category::find($id); 
 		$category ->publication_status = 0;
 		$category->save();

 		return redirect('/category/manage')->with('message', 'Category Info Unpublished');
 	}
 	public function publishedCategoryinfo($id){

 		$category = Category::find($id);
 		$category ->publication_status = 1;
 		$category->save();

 		return redirect('/category/manage')->with('message', 'Category Info published');
 	}
 		public function editCategoryinfo($id){

 		$category = Category::find($id);
 		
 		//return $category;

 		return view('admin.category.edit-category', ['category'  =>$category]);
 		//return redirect('/category/manage')->with('message', 'Category Info published');
 	}

 	//update Category info
 	public function updateCategoryinfo(Request $request){
 		//return $request->all();

 		$category = Category::find($request->category_id);

 		$category ->category_name      = $request->category_name;
 		$category ->category_dis       = $request->category_dis;
 		$category ->publication_status = $request->publication_status;
 		$category ->save();

 		return redirect('/category/manage')->with('message', 'Category Info Update Successfully');
 	}

 	//Delete Category

 	public function deleteCategoryinfo($id){

 		$category =Category::find($id);
 		$category->delete();

 		return redirect('/category/manage')->with('message', 'Category Info Delete Successfully');
 	}

}
