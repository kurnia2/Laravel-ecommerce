<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request; 

class NewShopController extends Controller
{
    public function index(){

       
       $newProducts = Product::where('publication_stutus',1)
                                    ->orderBy('id', 'DESC')
                                    ->take(6)
                                    ->get();
           // return $newProducts;
    	return view('front-end.home.home', [
            'newProducts'=>$newProducts
        ]);
    }

    public function category($id){
         $categoryProducts = Product::where('category_id',$id)
                                   ->where('publication_stutus',1)
                                   ->get();

    	return view('front-end.category.category-content',[
                'categoryProducts' => $categoryProducts
        ]);
    }
    //Product Details
    public function productdetails($id,$newProduct){
        $product =Product::find($id);
        return view('front-end.product.product-details',[
                    'product' => $product
        ]);

    }

    public function mail(){
    	return view('front-end.mail.mail-us');
    }
    public function login(){
    	return view('front-end.login.login-us');
    }
    public function checkout(){
    	return view('front-end.checkout.checkout-here');
    }
    public function register(){
    	return view('front-end.register.register-here');
    }

}
